from django.db import models
from django.contrib.auth.models import AbstractUser

class Doctor(models.Model):
    class Meta:
        db_table = 'doctor'

    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    email = models.EmailField(max_length=100, null=False)
    address = models.CharField(max_length=100, null=False)
    DNI = models.CharField(max_length=100, unique=True)
    cell = models.CharField(max_length=100, unique=True) 
    gender = models.CharField(max_length=20)
    school_number = models.IntegerField(unique=True)
    date_of_date = models.DateField()
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now_add=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.first_name


class Patient(models.Model):
    class Meta:
        db_table = 'patient'

    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    address = models.CharField(max_length=100, null=False)
    DNI = models.CharField(max_length=100, unique=True)
    cell = models.CharField(max_length=100, unique=True) 
    gender = models.CharField(max_length=20)
    date_of_date = models.DateField()
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now_add=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.first_name

class Appointment(models.Model):
    class Meta:
        db_table = 'appointment'
    
    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE, null=False)
    patient_id = models.ForeignKey(Patient, on_delete=models.CASCADE, null=False)
    date_attention = models.DateField()
    start_attention = models.DateTimeField(auto_now=True)
    finish_attention = models.DateTimeField(auto_now=True)
    state = models.CharField(max_length=100, null=False)
    observations = models.CharField(max_length=100, null=True)
    is_active = models.BooleanField(default=True)
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now=True, blank=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100)

    def __str__(self):
        return f'${doctor_id} ${patient_id}'

class Schedule(models.Model):
    class Meta:
            db_table = 'schedule'
    
    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE, null=False)
    date_attention = models.DateField()
    start_attention = models.DateTimeField(auto_now=True)
    finish_attention = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now=True, blank=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100) 

    def __str__(self):
        return f'${doctor_id} ${date_attention}'    

class Specialty(models.Model):
    class Meta:
            db_table = 'speciality'

    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=100, null=False)
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now_add=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100)     
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'${name} ${description}'

class Doctor_Specialty(models.Model):
    class Meta:
            db_table = 'doctor_specialty'

    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE, null=False)
    specialty_id = models.ForeignKey(Specialty, on_delete=models.CASCADE, null=False)
    date_register = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now_add=True)
    user_register = models.CharField(max_length=100)
    user_update = models.CharField(max_length=100)     
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'${doctor_id} ${specialty_id}'