from django.shortcuts import render
from doctor.serializers import *
from doctor.models import *
from rest_framework import viewsets, permissions

# Create your views here.


################# VIEWS REST_FRAMEWORK ###################

# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer
#     permission_classes = [permissions.IsAuthenticated]


class DoctorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows waiters to be viewed or edited.
    """
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer
    permission_classes = [permissions.IsAuthenticated]

class PatientViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Clients to be viewed or edited.
    """
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    permission_classes = [permissions.IsAuthenticated]

class AppointmentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Appointments to be viewed or edited.
    """
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    permission_classes = [permissions.IsAuthenticated]

class ScheduleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Schedules to be viewed or edited.
    """
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
    permission_classes = [permissions.IsAuthenticated]


class SpecialtyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Specialtys to be viewed or edited.
    """
    queryset = Specialty.objects.all()
    serializer_class = SpecialtySerializer
    permission_classes = [permissions.IsAuthenticated]

class DoctorSpecialtyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows DoctorSpecialtys to be viewed or edited.
    """
    queryset = Doctor_Specialty.objects.all()
    serializer_class = DoctorSpecialtySerializer
    permission_classes = [permissions.IsAuthenticated]
