from django.contrib.auth import get_user_model
from rest_framework import serializers
from doctor.models import *



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'username', 'firts_name', 'last_name', 'address', 'is_active']

class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = ['id', 'first_name', 'last_name', 'email', 'address', 'DNI', 'cell', 'gender', 'school_number', 'date_of_date', 'date_register','date_update','user_register','user_update','is_active']


class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = ['id', 'first_name', 'last_name', 'address', 'DNI', 'cell', 'gender', 'date_of_date', 'date_register','date_update','user_register','user_update','is_active']

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['id', 'doctor_id', 'patient_id', 'date_attention', 'start_attention', 'finish_attention', 'state', 'observations', 'date_register','date_update','user_register','user_update','is_active']

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['id', 'doctor_id', 'date_attention', 'start_attention','finish_attention','date_register','date_update','user_register','user_update','is_active']

class SpecialtySerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialty
        fields = ['id', 'name', 'description', 'date_register','date_update','user_register','user_update','is_active']

class DoctorSpecialtySerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor_Specialty
        fields = ['id', 'doctor_id', 'specialty_id', 'date_register','date_update','user_register','user_update','is_active']



