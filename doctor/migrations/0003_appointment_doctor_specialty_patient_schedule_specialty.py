# Generated by Django 3.2.13 on 2022-06-04 18:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('doctor', '0002_auto_20220604_1225'),
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('DNI', models.CharField(max_length=100, unique=True)),
                ('cell', models.CharField(max_length=100, unique=True)),
                ('gender', models.CharField(max_length=20)),
                ('date_of_date', models.DateField()),
                ('date_register', models.DateField(auto_now_add=True)),
                ('date_update', models.DateField()),
                ('user_register', models.CharField(max_length=100)),
                ('user_update', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'patient',
            },
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=100)),
                ('date_register', models.DateField(auto_now_add=True)),
                ('date_update', models.DateField()),
                ('user_register', models.CharField(max_length=100)),
                ('user_update', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'speciality',
            },
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_attention', models.DateField()),
                ('start_attention', models.DateTimeField(null=True)),
                ('finish_attention', models.DateTimeField(null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('date_register', models.DateField(auto_now_add=True)),
                ('date_update', models.DateField()),
                ('user_register', models.CharField(max_length=100)),
                ('user_update', models.CharField(max_length=100)),
                ('doctor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='doctor.doctor')),
            ],
            options={
                'db_table': 'schedule',
            },
        ),
        migrations.CreateModel(
            name='Doctor_Specialty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_register', models.DateField(auto_now_add=True)),
                ('date_update', models.DateField()),
                ('user_register', models.CharField(max_length=100)),
                ('user_update', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=True)),
                ('doctor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='doctor.doctor')),
                ('specialty_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='doctor.specialty')),
            ],
            options={
                'db_table': 'doctor_specialty',
            },
        ),
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_attention', models.DateField()),
                ('start_attention', models.DateTimeField(null=True)),
                ('finish_attention', models.DateTimeField(null=True)),
                ('state', models.CharField(max_length=100)),
                ('observations', models.CharField(max_length=100, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('date_register', models.DateField(auto_now_add=True)),
                ('date_update', models.DateField()),
                ('user_register', models.CharField(max_length=100)),
                ('user_update', models.CharField(max_length=100)),
                ('doctor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='doctor.doctor')),
                ('patient_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='doctor.patient')),
            ],
            options={
                'db_table': 'appointment',
            },
        ),
    ]
