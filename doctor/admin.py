from django.contrib import admin
from django.contrib.auth.models import Permission
from doctor.models import *

# Register your models here.

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = (
        'email',
        'first_name',
        'last_name',
        'is_active',
    )
    search_fields = (
        'first_name',
    )

@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    list_display = (
        'first_name',
        'last_name',
        'is_active',
    )
    search_fields = (
        'first_name',
    )

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = (
        'doctor_id',
        'patient_id',
        'date_attention',
        'is_active',
    )
    search_fields = (
        'doctor_id__first_name',
    )

@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = (
        'doctor_id',
        'date_attention',
        'start_attention',
    )
    search_fields = (
        'doctor_id__first_name',
        'patient_id__first_name',
    )

@admin.register(Specialty)
class SpecialtyAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'is_active',
    )
    search_fields = (
        'name',
    )

@admin.register(Doctor_Specialty)
class DoctorSpecialtyAdmin(admin.ModelAdmin):
    list_display = (
        'doctor_id',
        'specialty_id',
        'is_active',
    )
    search_fields = (
        'doctor_id__first_name',
        'apecialty_id__name',
    )


